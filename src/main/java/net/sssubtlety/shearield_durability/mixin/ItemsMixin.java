package net.sssubtlety.shearield_durability.mixin;


import net.minecraft.item.Items;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.ModifyArg;
import org.spongepowered.asm.mixin.injection.Slice;

import static net.sssubtlety.shearield_durability.FeatureControl.getShearsDurability;
import static net.sssubtlety.shearield_durability.FeatureControl.getShieldDurability;

@Mixin(Items.class)
public abstract class ItemsMixin {
    @ModifyArg(
            method = "<clinit>",
            at = @At(value = "INVOKE",
            target = "Lnet/minecraft/item/Item$Settings;maxDamage(I)Lnet/minecraft/item/Item$Settings;"),
            slice = @Slice(
                    from = @At(value = "CONSTANT", args = "stringValue=shears"),
                    to = @At(value = "CONSTANT", args = "stringValue=melon_slice")
            )
    )
    private static int modifyShearsDurability(int originalDurability) {
        return getShearsDurability();
    }

    @ModifyArg(
            method = "<clinit>",
            at = @At(value = "INVOKE",
                    target = "Lnet/minecraft/item/Item$Settings;maxDamage(I)Lnet/minecraft/item/Item$Settings;"),
            slice = @Slice(
                    from = @At(value = "CONSTANT", args = "stringValue=shield"),
                    to = @At(value = "CONSTANT", args = "stringValue=totem_of_undying")
            )
    )
    private static int modifyShieldDurability(int originalDurability) {
        return getShieldDurability();
    }
}
