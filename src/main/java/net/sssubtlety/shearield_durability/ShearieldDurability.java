package net.sssubtlety.shearield_durability;

import de.guntram.mcmod.crowdintranslate.CrowdinTranslate;
import net.fabricmc.api.ClientModInitializer;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.fabricmc.api.ModInitializer;

import static net.sssubtlety.shearield_durability.FeatureControl.shouldFetchTranslationUpdates;

public class ShearieldDurability {
	public static final String NAMESPACE = "mod_id";

	public static class Init implements ModInitializer {
		@Override
		public void onInitialize () {
			FeatureControl.init();
		}
	}

	public static class ClientInit implements ClientModInitializer {
		@Override
		@Environment(EnvType.CLIENT)
		public void onInitializeClient() {
			if (shouldFetchTranslationUpdates())
				CrowdinTranslate.downloadTranslations("mod-id", NAMESPACE);
		}
	}
}
