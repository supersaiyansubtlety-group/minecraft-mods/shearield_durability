package net.sssubtlety.shearield_durability;

import me.shedaniel.autoconfig.ConfigData;
import me.shedaniel.autoconfig.annotation.ConfigEntry;

import static net.sssubtlety.shearield_durability.ShearieldDurability.NAMESPACE;

@me.shedaniel.autoconfig.annotation.Config(name = NAMESPACE)
public class Config implements ConfigData {
    int shears_durability = FeatureControl.Defaults.shears_durability;

    int shield_durability = FeatureControl.Defaults.shield_durability;

    @ConfigEntry.Gui.Tooltip()
    boolean fetch_translation_updates = FeatureControl.Defaults.fetch_translation_updates;
}
