package net.sssubtlety.shearield_durability;

import me.shedaniel.autoconfig.AutoConfig;
import me.shedaniel.autoconfig.serializer.GsonConfigSerializer;
import net.fabricmc.loader.api.FabricLoader;
import net.fabricmc.loader.api.ModContainer;
import net.fabricmc.loader.api.VersionParsingException;
import net.fabricmc.loader.api.metadata.version.VersionPredicate;

import java.util.Optional;

public class FeatureControl {
    private static final Config CONFIG_INSTANCE;

    static {
        boolean shouldLoadConfig = false;

        final Optional<ModContainer> optModContainer = FabricLoader.getInstance().getModContainer("cloth-config");
        if (optModContainer.isPresent()){
            try {
                shouldLoadConfig = VersionPredicate.parse(">=6.1.48").test(optModContainer.get().getMetadata().getVersion());
            } catch (VersionParsingException e) {
                e.printStackTrace();
            }
        }

        CONFIG_INSTANCE = shouldLoadConfig ?
                AutoConfig.register(Config.class, GsonConfigSerializer::new).getConfig() : null;

    }

    public interface Defaults {
        int shears_durability = 500;
        int shield_durability = 500;
        boolean fetch_translation_updates = true;
    }

    int shears_durability = 500;
    int shield_durability = 500;

    public static int getShearsDurability() {
        return isConfigLoaded() ? CONFIG_INSTANCE.shears_durability : Defaults.shears_durability;
    }

    public static int getShieldDurability() {
        return isConfigLoaded() ? CONFIG_INSTANCE.shield_durability : Defaults.shield_durability;
    }

    public static boolean shouldFetchTranslationUpdates() {
        return isConfigLoaded() ? CONFIG_INSTANCE.fetch_translation_updates : Defaults.fetch_translation_updates;
    }

    public static boolean isConfigLoaded() {
        return CONFIG_INSTANCE != null;
    }

    public static void init() { }

    private FeatureControl() { }
}
