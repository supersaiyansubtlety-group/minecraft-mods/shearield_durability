<div align="center">

This mod is for the [Fabric mod loader](https://www.fabricmc.net/) and <a href="https://www.curseforge.com/minecraft/mc-mods/shearield-durability/files"><img style="vertical-align:middle" src="https://cf.way2muchnoise.eu/versions/Minecraft_mod-id_all.svg" alt="Minecraft Versions"></a>

<a href="https://modrinth.com/mod/fabric-api/versions"><img src="https://i.imgur.com/Ol1Tcf8.png" alt="Requires Fabric API" width="300"></a>

Supports [Cloth Config](https://www.curseforge.com/minecraft/mc-mods/cloth-config/files) and [Mod Menu](https://www.curseforge.com/minecraft/mc-mods/modmenu/files) for configuration, but neither is required.

Download the mod on [Modrinth](https://modrinth.com/mod/shearield-durability) or [CurseForge](https://www.curseforge.com/minecraft/mc-mods/shearield-durability)!

Like this mod?
<a href="https://coindrop.to/supersaiyansubtlety"><img width="70" style="vertical-align:middle" src="https://coindrop.to/embed-button.png" alt="Coindrop.to me"></a> or
<a href="https://ko-fi.com/supersaiyansubtlety"><img width="90" style="vertical-align:middle" src="https://i.ibb.co/4gwRR8L/p.png" alt="Buy me a coffee"></a>

<a href="hhttps://will-lucic.mit-license.org/"><img style="vertical-align:middle" src="https://img.shields.io/badge/license-MIT-green" alt="License MIT"></a>
<a href="https://www.curseforge.com/minecraft/mc-mods/shearield-durability/files"><img style="vertical-align:middle" src="https://cf.way2muchnoise.eu/full_mod-id_downloads.svg" alt="CurseForge Downloads"></a>

</div>

## This is a template mod.



#### Features


  
------

<details>

<summary>Configuration</summary>

Configuration requires [Cloth Config](https://www.curseforge.com/minecraft/mc-mods/cloth-config/files),
and configuring the mod in-game requires [Mod Menu](https://modrinth.com/mod/modmenu/versions).

##### Options:

- Download translation updates: Download translations from Crowdin when the game launches

</details>

It's required Shearield Durability be installed on both client and server.

<details>

<summary>Credits</summary>

- [glisco](https://github.com/glisco03) for making [Isometric Renders](https://github.com/glisco03/isometric-renders), which I used to create the mod icon.

</details>

This mod is only for Fabric and I won't be porting it to Forge. The license is [MIT](https://will-lucic.mit-license.org/), however, so anyone else is free to port it.

I'd appreciate links back to this page if you port or otherwise modify this project, but links aren't required. 
